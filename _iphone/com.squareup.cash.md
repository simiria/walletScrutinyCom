---
wsId: CashApp
title: Cash App
altTitle: 
authors:
- leo
appId: com.squareup.cash
appCountry: 
idd: 711923939
released: 2013-10-16
updated: 2023-11-28
version: '4.23'
stars: 4.8
reviews: 5594045
size: '339081216'
website: https://cash.app
repository: 
issue: 
icon: com.squareup.cash.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2020-12-22
signer: 
reviewArchive: 
twitter: cashapp
social: 
features: 
developerName: Block, Inc.

---

On their website the provider claims:

> **Coin Storage**<br>
  Your Bitcoin balance is securely stored in our offline system

which means it is custodial.
