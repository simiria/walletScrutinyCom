---
wsId: Vidulum
title: Vidulum
altTitle: 
authors:
- leo
appId: com.vidulum.app
appCountry: 
idd: 1505859171
released: 2020-07-28
updated: 2023-07-20
version: 1.5.3
stars: 4.3
reviews: 12
size: '69446656'
website: https://vidulum.app
repository: 
issue: 
icon: com.vidulum.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: VidulumApp
social:
- https://www.facebook.com/VidulumTeam
- https://www.reddit.com/r/VidulumOfficial
features: 
developerName: Vidulum LLC

---

{% include copyFromAndroid.html %}
