---
wsId: xverse
title: Xverse - Bitcoin Wallet
altTitle: 
authors:
- danny
appId: com.secretkeylabs.xverse
appCountry: gt
idd: 1552272513
released: 2021-10-15
updated: 2023-11-27
version: v1.22.0
stars: 5
reviews: 1
size: '36049920'
website: https://twitter.com/xverseApp
repository: 
issue: 
icon: com.secretkeylabs.xverse.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2022-01-13
signer: 
reviewArchive: 
twitter: secretkeylabs
social: 
features: 
developerName: Secret Key Labs

---

{% include copyFromAndroid.html %}

