---
wsId: bipaBR
title: Bipa - Pix, Bitcoin & USDT
altTitle: 
authors:
- danny
appId: bipa.app.Bipa
appCountry: br
idd: '1516842324'
released: 2020-06-30
updated: 2023-11-24
version: 3.3.0
stars: 4.6
reviews: 1004
size: '101340160'
website: https://bipa.app
repository: 
issue: 
icon: bipa.app.Bipa.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-06-14
signer: 
reviewArchive: 
twitter: usebipa
social: 
features: 
developerName: Bipa Ltda

---

{% include copyFromAndroid.html %}