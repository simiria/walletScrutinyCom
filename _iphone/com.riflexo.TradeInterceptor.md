---
wsId: ThinkTrader
title: ThinkTrader
altTitle: 
authors:
- danny
appId: com.riflexo.TradeInterceptor
appCountry: us
idd: 329476057
released: 2009-09-23
updated: 2023-11-15
version: 6.7.50
stars: 4.8
reviews: 474
size: '83785728'
website: https://www.thinkmarkets.com/uk/about-us/
repository: 
issue: 
icon: com.riflexo.TradeInterceptor.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-10-21
signer: 
reviewArchive: 
twitter: ThinkMarketscom
social:
- https://www.linkedin.com/company/thinkmarkets
- https://www.facebook.com/ThinkMarkets
features: 
developerName: ThinkMarkets

---

{% include copyFromAndroid.html %}
