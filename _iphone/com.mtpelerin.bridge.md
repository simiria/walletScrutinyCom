---
wsId: bridgeWallet
title: 'Bridge: Bitcoin Crypto Wallet'
altTitle: 
authors:
- danny
appId: com.mtpelerin.bridge
appCountry: us
idd: 1481859680
released: 2020-04-08
updated: 2023-11-13
version: '1.35'
stars: 4.2
reviews: 49
size: '111042560'
website: https://www.mtpelerin.com/bridge-wallet
repository: 
issue: 
icon: com.mtpelerin.bridge.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2022-01-10
signer: 
reviewArchive: 
twitter: mtpelerin
social:
- https://www.linkedin.com/company/mt-pelerin
- https://www.facebook.com/mtpelerin
- https://www.reddit.com/r/MtPelerin
features: 
developerName: Mt Pelerin

---

{% include copyFromAndroid.html %}
