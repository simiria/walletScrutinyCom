---
wsId: bitfy
title: Bitfy
altTitle: 
authors:
- danny
appId: app.bitfy
appCountry: us
idd: 1483269793
released: 2019-11-26
updated: 2023-11-22
version: 3.12.39
stars: 2.8
reviews: 5
size: '56885248'
website: https://bitfy.app
repository: 
issue: 
icon: app.bitfy.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-09-03
signer: 
reviewArchive: 
twitter: bitfyapp
social:
- https://www.facebook.com/bitfyapp
features: 
developerName: WARP INTERMEDIACOES E SERVICOS DE PAGAMENTO LTDA

---

{% include copyFromAndroid.html %}
