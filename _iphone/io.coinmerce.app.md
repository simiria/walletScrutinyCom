---
wsId: coinmerce
title: Coinmerce - Bitcoin Kopen
altTitle: 
authors:
- danny
appId: io.coinmerce.app
appCountry: nl
idd: '1409599830'
released: 2018-07-29
updated: 2023-11-03
version: 5.7.5
stars: 2.5
reviews: 86
size: '186804224'
website: https://coinmerce.io/en/
repository: 
issue: 
icon: io.coinmerce.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-15
signer: 
reviewArchive: 
twitter: coinmerce
social:
- https://www.facebook.com/CoinmerceNL
- https://www.linkedin.com/company/coinmerce/
features: 
developerName: Coinmerce BV

---

{% include copyFromAndroid.html %}

