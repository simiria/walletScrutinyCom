---
wsId: zabiraBuyBTC
title: 'Zabira: Buy BTC, ETH & Crypto'
altTitle: 
authors:
- danny
appId: app.zabira.ng
appCountry: us
idd: '1603796325'
released: 2022-04-05
updated: 2023-10-18
version: 1.0.28.1
stars: 2.9
reviews: 43
size: '63065088'
website: 
repository: 
issue: 
icon: app.zabira.ng.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-18
signer: 
reviewArchive: 
twitter: thezabira
social:
- https://www.facebook.com/thezabira
- https://www.instagram.com/thezabira
- https://www.linkedin.com/company/zabira/about
features: 
developerName: Zabira Technologies

---

{% include copyFromAndroid.html %}