---
wsId: wexoCrypto
title: 'WEXO: Bitcoin & Crypto Wallet'
altTitle: 
authors:
- danny
appId: com.wexopay.app
appCountry: sk
idd: '1549983870'
released: 2021-02-09
updated: 2023-11-22
version: 2.5.22
stars: 4.8
reviews: 202
size: '70115328'
website: https://wexopay.com
repository: 
issue: 
icon: com.wexopay.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-06-30
signer: 
reviewArchive: 
twitter: WexoOfficial
social:
- https://www.facebook.com/wexopay
- https://t.me/WexoToken
- https://www.youtube.com/channel/UCZhC-OF4j0I5ls51EjKT1UA
- https://discord.com/invite/jhqwr7KFCn
features: 
developerName: UPDN ONE s.r.o.

---

{% include copyFromAndroid.html %}
