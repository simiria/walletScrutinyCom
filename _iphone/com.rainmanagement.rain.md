---
wsId: rainfinancial
title: 'Rain: Buy & Sell Bitcoin'
altTitle: 
authors:
- danny
appId: com.rainmanagement.rain
appCountry: bh
idd: 1414619890
released: 2018-09-02
updated: 2023-11-28
version: 3.4.1
stars: 4.7
reviews: 2428
size: '92199936'
website: https://www.rain.bh/
repository: 
issue: 
icon: com.rainmanagement.rain.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: rainfinancial
social:
- https://www.linkedin.com/company/rainfinancial
- https://www.facebook.com/rainfinancial
features: 
developerName: Rain Management

---

 {% include copyFromAndroid.html %}
