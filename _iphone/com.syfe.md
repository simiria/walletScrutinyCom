---
wsId: syfeInvest
title: 'Syfe: Stay Invested'
altTitle: 
authors:
- danny
appId: com.syfe
appCountry: sg
idd: '1497156434'
released: 2020-02-24
updated: 2023-11-29
version: 9.9.1
stars: 4.2
reviews: 575
size: '108648448'
website: https://www.syfe.com
repository: 
issue: 
icon: com.syfe.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-07-10
signer: 
reviewArchive: 
twitter: SyfeSG
social:
- https://www.linkedin.com/company/syfe
- https://www.facebook.com/SyfeSG
- https://www.instagram.com/SyfeSG
- https://www.youtube.com/c/SyfeSG
features: 
developerName: Syfe Pte. Ltd.

---

{% include copyFromAndroid.html %}
