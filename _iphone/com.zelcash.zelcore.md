---
wsId: ZelCore
title: ZelCore
altTitle: 
authors:
- leo
appId: com.zelcash.zelcore
appCountry: 
idd: 1436296839
released: 2018-09-23
updated: 2023-09-13
version: 7.5.0
stars: 3.7
reviews: 3
size: '95084544'
website: https://zelcore.io
repository: 
issue: 
icon: com.zelcash.zelcore.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: zelcash
social:
- https://www.reddit.com/r/ZelCash
features: 
developerName: Zelcore Technologies Inc.

---

{% include copyFromAndroid.html %}
