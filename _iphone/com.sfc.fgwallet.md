---
wsId: ivanFGWallet
title: FG Wallet
altTitle: 
authors:
- danny
appId: com.sfc.fgwallet
appCountry: us
idd: '1338808692'
released: 2018-02-27
updated: 2023-11-14
version: 3.7.8
stars: 5
reviews: 2
size: '34809856'
website: https://srsfc.com/
repository: 
issue: 
icon: com.sfc.fgwallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2022-06-30
signer: 
reviewArchive: 
twitter: srsfintech
social:
- https://www.linkedin.com/company/srsfintech/
features: 
developerName: SRS FINTECH COMMERCE LTD

---

{% include copyFromAndroid.html %}
