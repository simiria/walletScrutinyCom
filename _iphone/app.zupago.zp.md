---
wsId: ZuPago
title: ZuPago
altTitle: 
authors:
- danny
appId: app.zupago.zp
appCountry: us
idd: 1565673730
released: 2021-05-10
updated: 2023-10-08
version: 1.0.56
stars: 4.5
reviews: 50
size: '31745024'
website: https://zupago.app
repository: 
issue: 
icon: app.zupago.zp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-12-19
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: ZuPago HyBrid HD Wallet

---

{% include copyFromAndroid.html %}