---
wsId: 
title: Cryptnox Wallet
altTitle: 
authors:
- danny
appId: com.cryptnox.companion
appCountry: us
idd: '1583011693'
released: 2021-10-10
updated: 2023-11-12
version: 2.2.0
stars: 3.7
reviews: 3
size: '65463296'
website: https://www.cryptnox.com
repository: 
issue: 
icon: com.cryptnox.companion.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2022-11-26
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Cryptnox

---

This is the companion app to the {% include walletLink.html wallet='hardware/cryptnox.bg1card' verdict='true' %} hardware wallet.