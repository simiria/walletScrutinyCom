---
wsId: fairdesk
title: Fairdesk Go:Trade Bitcoin, ETH
altTitle: 
authors:
- danny
appId: com.Fairdesk.fairdesk
appCountry: us
idd: '1578440544'
released: 2021-08-18
updated: 2023-08-08
version: 1.5.3
stars: 4.7
reviews: 29
size: '23473152'
website: https://www.fairdesk.com
repository: 
issue: 
icon: com.Fairdesk.fairdesk.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-09
signer: 
reviewArchive: 
twitter: FairdeskGlobal
social:
- https://www.reddit.com/user/Fairdesk_Official
- https://www.youtube.com/channel/UCYcwULjw4ci90htneCrMu1w
features: 
developerName: FAIRDESK TECHNOLOGY LIMITED

---

{% include copyFromAndroid.html %}

