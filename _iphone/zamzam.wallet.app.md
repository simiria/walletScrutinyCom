---
wsId: ZamWallet
title: ZamWallet Crypto, DeFi, Invest
altTitle: 
authors:
- danny
appId: zamzam.wallet.app
appCountry: ru
idd: 1436344249
released: 2018-10-17
updated: 2023-11-30
version: 3.3.5
stars: 4.2
reviews: 73
size: '47510528'
website: https://zam.io
repository: 
issue: 
icon: zamzam.wallet.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-11-04
signer: 
reviewArchive: 
twitter: zam_io
social:
- https://www.linkedin.com/company/11770701
features: 
developerName: ZAMZAMTECHNOLOGY OU

---

{% include copyFromAndroid.html %}
