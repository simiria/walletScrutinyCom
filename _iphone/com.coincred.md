---
wsId: coinCRED
title: CoinCRED
altTitle: 
authors:
- danny
appId: com.coincred
appCountry: in
idd: '1596466402'
released: 2021-12-31
updated: 2023-11-19
version: 2.0.8
stars: 3.4
reviews: 106
size: '41321472'
website: https://coincred.org/
repository: 
issue: 
icon: com.coincred.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-06-27
signer: 
reviewArchive: 
twitter: CoinCRED_
social:
- https://www.facebook.com/CoinCRED.Official/
- https://www.linkedin.com/company/coin-cred/
features: 
developerName: CoinCRED

---

{% include copyFromAndroid.html %}