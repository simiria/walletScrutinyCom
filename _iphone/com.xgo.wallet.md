---
wsId: xGoWallet
title: XGo - buy, swap & store crypto
altTitle: 
authors:
- danny
appId: com.xgo.wallet
appCountry: bg
idd: '1630753422'
released: 2022-11-03
updated: 2023-11-27
version: 1.13.0
stars: 0
reviews: 0
size: '105708544'
website: https://xgo.com
repository: 
issue: 
icon: com.xgo.wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-17
signer: 
reviewArchive: 
twitter: XGo_official
social:
- https://t.me/+Z536yScjjQg0NzE0
- https://www.instagram.com/xgo_official
- https://discord.com/invite/dTt4Ke3
features: 
developerName: Exechain OU

---

{% include copyFromAndroid.html %}
