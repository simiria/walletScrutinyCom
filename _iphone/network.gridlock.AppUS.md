---
wsId: GridLock
title: 'Gridlock: Secure Crypto Wallet'
altTitle: 
authors:
- danny
appId: network.gridlock.AppUS
appCountry: us
idd: '1567057330'
released: '2021-06-07'
updated: 2023-11-23
version: 2.1.8
stars: 4.8
reviews: 17
size: '77364224'
website: https://gridlock.network
repository: 
issue: 
icon: network.gridlock.AppUS.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2022-12-08
signer: 
reviewArchive: 
twitter: GridlockCrypto
social:
- https://www.facebook.com/GridlockNetwork
features: 
developerName: Gridlock

---

{% include copyFromAndroid.html %}