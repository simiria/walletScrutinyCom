---
wsId: webAuthProton
title: WebAuth
altTitle: 
authors:
- danny
appId: com.metallicus.webauth
appCountry: us
idd: '1594500069'
released: 2021-12-15
updated: 2023-11-21
version: 2.1.1
stars: 4.8
reviews: 161
size: '34973696'
website: https://xprnetwork.org/
repository: 
issue: 
icon: com.metallicus.webauth.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-06
signer: 
reviewArchive: 
twitter: protonxpr
social:
- https://www.facebook.com/protonxpr
- https://www.reddit.com/r/ProtonChain
- https://t.me/protonxpr
- https://discord.com/invite/B2QDmgf
features: 
developerName: Metallicus, Inc.

---

{% include copyFromAndroid.html %}
