---
wsId: BinanceTR
title: Binance TR:Buy Crypto with TRY
altTitle: 
authors:
- danny
appId: com.binanceCloudTR.binance
appCountry: tr
idd: 1548636153
released: 2021-02-18
updated: 2023-11-23
version: 2.2.1
stars: 4.6
reviews: 67241
size: '133332992'
website: https://www.trbinance.com/
repository: 
issue: 
icon: com.binanceCloudTR.binance.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive: 
twitter: BinanceTR
social:
- https://www.facebook.com/TRBinanceTR
features: 
developerName: BN TEKNOLOJİ ANONİM ŞİRKETİ

---

{% include copyFromAndroid.html %}
