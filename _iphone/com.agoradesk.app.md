---
wsId: agoraDeskAnonymous
title: 'AgoraDesk: buy Bitcoin easily'
altTitle: 
authors:
- danny
appId: com.agoradesk.app
appCountry: us
idd: '1617601678'
released: 2022-08-19
updated: 2023-11-30
version: 1.1.15
stars: 5
reviews: 8
size: '52209664'
website: https://agoradesk.com
repository: 
issue: 
icon: com.agoradesk.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-17
signer: 
reviewArchive: 
twitter: AgoraDesk
social:
- https://www.reddit.com/r/AgoraDesk
- https://t.me/AgoraDesk
features: 
developerName: Blue Sunday Limited

---

{% include copyFromAndroid.html %}