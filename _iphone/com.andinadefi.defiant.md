---
wsId: Defiant
title: Defiant Wallet
altTitle: 
authors:
- danny
appId: com.andinadefi.defiant
appCountry: ar
idd: 1559622756
released: 2021-04-07
updated: 2023-11-07
version: 5.3.3+300
stars: 4.7
reviews: 20
size: '87974912'
website: https://defiantapp.tech/
repository: 
issue: 
icon: com.andinadefi.defiant.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-11-15
signer: 
reviewArchive: 
twitter: defiantApp
social: 
features: 
developerName: Andina Defi Ltd

---

{% include copyFromAndroid.html %}
