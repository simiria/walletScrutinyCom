---
wsId: xfacia
title: Xfacia
altTitle: 
authors:
- danny
appId: com.webcom.Xfacia
appCountry: br
idd: '1620527067'
released: 2022-08-15
updated: 2022-12-21
version: '4.1'
stars: 0
reviews: 0
size: '82309120'
website: https://xfacia.com/
repository: 
issue: 
icon: com.webcom.Xfacia.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-13
signer: 
reviewArchive: 
twitter: XfaciaXchange
social:
- https://www.facebook.com/XFACIAXCHANGE
- https://www.instagram.com/p/CTeDT7Ui0Ee
- https://t.me/XfaciaXchange
features: 
developerName: Xfacia Labs Private Limited

---

{% include copyFromAndroid.html %}

