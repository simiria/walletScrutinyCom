---
wsId: getDelta
title: Delta Investment Tracker
altTitle: 
authors: 
appId: io.getdelta.ios
appCountry: us
idd: 1288676542
released: 2017-09-25
updated: 2023-11-23
version: 2023.8.2
stars: 4.7
reviews: 10564
size: '121712640'
website: https://delta.app
repository: 
issue: 
icon: io.getdelta.ios.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-11-01
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Opus Labs CVBA

---

{% include copyFromAndroid.html %}
