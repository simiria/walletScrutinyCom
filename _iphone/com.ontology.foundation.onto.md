---
wsId: ONTO
title: ONTO-Cross-chain Crypto Wallet
altTitle: 
authors:
- danny
appId: com.ontology.foundation.onto
appCountry: us
idd: 1436009823
released: 2018-09-21
updated: 2023-10-23
version: 4.5.6
stars: 4
reviews: 81
size: '267426816'
website: https://www.onto.app
repository: 
issue: 
icon: com.ontology.foundation.onto.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-09-15
signer: 
reviewArchive: 
twitter: ONTOWallet
social: 
features: 
developerName: Ontology Foundation

---

{% include copyFromAndroid.html %}