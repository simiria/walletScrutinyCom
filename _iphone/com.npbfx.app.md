---
wsId: npbfxCFD
title: NPBFX
altTitle: 
authors:
- danny
appId: com.npbfx.app
appCountry: ph
idd: '1541838401'
released: 2022-04-21
updated: 2022-12-07
version: 1.3.9
stars: 0
reviews: 0
size: '25222144'
website: https://www.npbfx.com/
repository: 
issue: 
icon: com.npbfx.app.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-07-03
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: NMarkets Limited

---

{% include copyFromAndroid.html %}
