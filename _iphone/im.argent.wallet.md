---
wsId: argent
title: Argent — Your Starknet Wallet
altTitle: 
authors:
- danny
appId: im.argent.wallet
appCountry: us
idd: 1358741926
released: 2018-10-25
updated: 2023-11-23
version: 4.21.2
stars: 4.6
reviews: 2125
size: '130756608'
website: https://www.argent.xyz
repository: 
issue: 
icon: im.argent.wallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2022-01-10
signer: 
reviewArchive: 
twitter: argentHQ
social: 
features: 
developerName: Argent

---

{% include copyFromAndroid.html %}