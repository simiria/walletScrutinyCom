---
wsId: rockitCoin
title: RockItCoin - Buy Bitcoin Now
altTitle: 
authors:
- danny
appId: rockitcoinexchange
appCountry: us
idd: '1476730078'
released: 2019-09-18
updated: 2023-11-16
version: 3.3.1
stars: 3.2
reviews: 120
size: '116885504'
website: https://rockitcoin.com
repository: 
issue: 
icon: rockitcoinexchange.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-07
signer: 
reviewArchive: 
twitter: rockitcoin
social:
- https://www.facebook.com/RockItCoin
- https://www.instagram.com/rockitcoin
features: 
developerName: RockitCoin

---

{% include copyFromAndroid.html %}
