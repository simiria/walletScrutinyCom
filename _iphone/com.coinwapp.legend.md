---
wsId: coinWBuyCrypto
title: CoinW
altTitle: 
authors:
- danny
appId: com.coinwapp.legend
appCountry: us
idd: '1494077068'
released: 2020-01-14
updated: 2023-04-19
version: 9.5.26
stars: 3.3
reviews: 83
size: '174373888'
website: 
repository: 
issue: 
icon: com.coinwapp.legend.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-07
signer: 
reviewArchive: 
twitter: CoinWOfficial
social:
- https://www.coinw.com
- https://t.me/CoinwExchangeEnglish
- https://www.linkedin.com/company/coinw-exchange
- https://medium.com/@coinwglobal23
- https://www.instagram.com/coinw_exchange
features: 
developerName: LCT HOLDING SP Z O O

---

{% include copyFromAndroid.html %}
