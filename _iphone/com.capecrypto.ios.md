---
wsId: capeCryptoExchange
title: Cape Crypto Exchange
altTitle: 
authors:
- danny
appId: com.capecrypto.ios
appCountry: za
idd: '1588237941'
released: 2021-10-18
updated: 2023-11-13
version: 2.4.5
stars: 5
reviews: 4
size: '125749248'
website: https://capecrypto.com
repository: 
issue: 
icon: com.capecrypto.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-09-06
signer: 
reviewArchive: 
twitter: capecryptoSA
social:
- https://www.linkedin.com/company/capecrypto
- https://www.youtube.com/channel/UCET8t88fPnOKD0sgM3KFIsA
- https://www.facebook.com/capecrypto
features: 
developerName: Cape Crypto Pty Ltd

---

{% include copyFromAndroid.html %}