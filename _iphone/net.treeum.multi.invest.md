---
wsId: treeumMultiInvest
title: Multi Invest
altTitle: 
authors:
- danny
appId: net.treeum.multi.invest
appCountry: ro
idd: '1587744083'
released: 2022-04-07
updated: 2022-09-01
version: 1.0.31
stars: 0
reviews: 0
size: '109927424'
website: https://multi.ua/
repository: 
issue: 
icon: net.treeum.multi.invest.jpg
bugbounty: 
meta: stale
verdict: wip
date: 2023-09-08
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Treeum Holdings LTD

---

{% include copyFromAndroid.html %}