---
wsId: satoshiBitcoinLightning
title: Satoshi Bitcoin Lightning
altTitle: 
authors:
- danny
appId: app.vipsats.satos
appCountry: tt
idd: '6445799528'
released: 2023-03-27
updated: 2023-09-03
version: 1.3.1
stars: 0
reviews: 0
size: '2475008'
website: https://vipsats.app
repository: 
issue: 
icon: app.vipsats.satos.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2023-09-06
signer: 
reviewArchive: 
twitter: vipsats
social: 
features: 
developerName: VIPSATS.APP

---

{% include copyFromAndroid.html %}