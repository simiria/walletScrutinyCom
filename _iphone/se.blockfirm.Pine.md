---
wsId: 
title: Pine – Bitcoin Wallet
altTitle: 
authors: 
appId: se.blockfirm.Pine
appCountry: 
idd: 1445357961
released: 2019-12-29
updated: 2022-07-21
version: 1.3.6
stars: 5
reviews: 2
size: '18202624'
website: https://pine.pm
repository: 
issue: 
icon: se.blockfirm.Pine.jpg
bugbounty: 
meta: stale
verdict: wip
date: 2023-07-17
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Pine Enterprises AB

---

