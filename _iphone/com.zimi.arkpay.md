---
wsId: arkpayBTCETHWallet
title: ArkPay - BTC ETH Wallet
altTitle: 
authors:
- danny
appId: com.zimi.arkpay
appCountry: tw
idd: '1490824690'
released: 2019-12-29
updated: 2023-11-25
version: 2.13.0
stars: 3.1
reviews: 9
size: '35357696'
website: 
repository: 
issue: 
icon: com.zimi.arkpay.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-08
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Zimi

---

{% include copyFromAndroid.html %}
