---
wsId: swapsCryptoApp
title: swaps.app
altTitle: 
authors:
- danny
appId: com.swaps.only
appCountry: xk
idd: '1567210224'
released: 2021-05-31
updated: 2023-08-29
version: 2.1.3
stars: 0
reviews: 0
size: '148473856'
website: 
repository: 
issue: 
icon: com.swaps.only.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-08-09
signer: 
reviewArchive: 
twitter: swapsapp_
social:
- https://www.swaps.app
- https://www.linkedin.com/company/swaps-app
- https://www.instagram.com/swaps.app
- https://www.facebook.com/swapapp
- https://www.reddit.com/r/SwapsApp
features: 
developerName: OCTO LIQUIDITY OU

---

{% include copyFromAndroid.html %}