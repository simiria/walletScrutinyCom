---
wsId: weexTrade
title: WEEX - Buy Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: com.wake.weexprd
appCountry: us
idd: '1609350789'
released: 2022-03-04
updated: 2023-11-16
version: 3.1.1
stars: 2.7
reviews: 21
size: '141678592'
website: https://www.weex.com/en
repository: 
issue: 
icon: com.wake.weexprd.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-17
signer: 
reviewArchive: 
twitter: WEEX_Official
social:
- https://www.facebook.com/WEEXGlobal
- https://t.me/Weex_Global
- https://www.linkedin.com/company/weex-global
features: 
developerName: Wake Co., Ltd.
redirect_from: 

---

{% include copyFromAndroid.html %}