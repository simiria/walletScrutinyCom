---
wsId: rakutenCrypto
title: 楽天ウォレットアプリ
altTitle: 
authors:
- danny
appId: jp.co.rakuten.wallet.crypto
appCountry: jp
idd: '1472320399'
released: 2019-08-31
updated: 2023-11-15
version: 1.4.7
stars: 4.2
reviews: 6402
size: '37515264'
website: https://www.rakuten-wallet.co.jp/
repository: 
issue: 
icon: jp.co.rakuten.wallet.crypto.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-29
signer: 
reviewArchive: 
twitter: Rakuten_Wallet
social: 
features: 
developerName: Rakuten Wallet, Inc.

---

{% include copyFromAndroid.html %}