---
wsId: Verso
title: Verso Wallet - Crypto & NFT
altTitle: 
authors:
- danny
appId: com.btuprotocol.btuWallet
appCountry: ba
idd: 1539304605
released: 2021-03-15
updated: 2023-11-29
version: 2.33.0
stars: 0
reviews: 0
size: '94650368'
website: https://get-verso.com
repository: 
issue: 
icon: com.btuprotocol.btuWallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-10-19
signer: 
reviewArchive: 
twitter: versoapp
social: 
features: 
developerName: 808 Labs

---

{% include copyFromAndroid.html %}
