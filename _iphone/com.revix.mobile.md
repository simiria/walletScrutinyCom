---
wsId: revixBuyBitcoin
title: Revix - Buy Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: com.revix.mobile
appCountry: za
idd: '1590491829'
released: 2022-08-01
updated: 2023-07-27
version: 1.6.0
stars: 4.2
reviews: 17
size: '101847040'
website: https://revix.com
repository: 
issue: 
icon: com.revix.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-09
signer: 
reviewArchive: 
twitter: RevixInvest
social:
- https://www.facebook.com/RevixInvest
- https://www.linkedin.com/company/revixinvest
- https://www.instagram.com/revixinvest
- https://medium.com/revixinvestment
- https://t.me/revixinvest
- https://www.tiktok.com/@revixinvest
features: 
developerName: Revix UK Ltd

---

{% include copyFromAndroid.html %}
