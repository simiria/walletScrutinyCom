---
wsId: ambotTrading
title: AM Bot
altTitle: 
authors:
- danny
appId: com.ambot.prod
appCountry: th
idd: '1584284794'
released: 2021-11-16
updated: 2022-01-20
version: 1.0.7
stars: 0
reviews: 0
size: '76312576'
website: 
repository: 
issue: 
icon: com.ambot.prod.jpg
bugbounty: 
meta: stale
verdict: nobtc
date: 2023-08-07
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: LIVINGSTONES TECHNOLOGY PTE LTD

---

{% include copyFromAndroid.html %}
