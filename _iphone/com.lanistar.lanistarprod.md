---
wsId: lanistar
title: Lanistar
altTitle: 
authors:
- danny
appId: com.lanistar.lanistarprod
appCountry: us
idd: '1535627210'
released: 2020-11-04
updated: 2023-11-21
version: 2.0.57
stars: 3.3
reviews: 108
size: '102528000'
website: https://www.lanistar.com
repository: 
issue: 
icon: com.lanistar.lanistarprod.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2023-07-10
signer: 
reviewArchive: 
twitter: iamlanistar
social:
- https://www.instagram.com/lanistar
features: 
developerName: Lanistar Limited

---

{% include copyFromAndroid.html %}
