---
wsId: senexPayBitcoin
title: 'SenexPay: Buy & Store Bitcoin'
altTitle: 
authors:
- danny
appId: com.senexpay.mobile
appCountry: us
idd: '1627864145'
released: 2022-08-18
updated: 2023-09-01
version: 1.5.1
stars: 4
reviews: 13
size: '63497216'
website: https://senexpay.com
repository: 
issue: 
icon: com.senexpay.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-09-07
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Senex Payment Services Limited

---

{% include copyFromAndroid.html %}
