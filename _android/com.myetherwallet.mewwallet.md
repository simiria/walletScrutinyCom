---
wsId: mewEthereum
title: 'MEW crypto wallet: DeFi Web3'
altTitle: 
authors:
- danny
users: 500000
appId: com.myetherwallet.mewwallet
appCountry: us
released: 2020-03-11
updated: 2023-11-15
version: 2.6.6
stars: 4.5
ratings: 7093
reviews: 576
size: 
website: http://mewwallet.com
repository: 
issue: 
icon: com.myetherwallet.mewwallet.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-02-05
signer: 
reviewArchive: 
twitter: myetherwallet
social:
- https://www.linkedin.com/company/myetherwallet
- https://www.facebook.com/MyEtherWallet
- https://www.reddit.com/r/MyEtherWallet
redirect_from: 
developerName: MyEtherWallet
features: 

---

Supports 3 chains: Ethereum, Binance and Polygon. 

Does not support BTC.