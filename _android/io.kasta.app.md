---
wsId: kastaCryptoPayments
title: 'Ka.app: Crypto Wallet'
altTitle: 
authors:
- danny
users: 5000
appId: io.kasta.app
appCountry: 
released: 
updated: 2023-09-25
version: 1.27.113-prod-1cbe0a6
stars: 
ratings: 
reviews: 
size: 
website: https://www.kasta.io/
repository: 
issue: 
icon: io.kasta.app.png
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-07
signer: 
reviewArchive: 
twitter: ka_app
social:
- https://t.me/kasta_app
- https://www.youtube.com/channel/UCJHVGV7RzYLSzqckFcGq-jQ
redirect_from: 
developerName: Kasta.io
features: 

---

## App Description from Google Play

> Kasta is a peer-to-peer (P2P) crypto payment app that allows you to deposit, withdraw, swap, receive, and send crypto easily and instantly. You don’t have to worry about logging into a p2p crypto exchange or managing a crypto wallet as you can receive or send Bitcoin and other crypto by simply scanning a QR code or using a mobile number.
>
> The Easy Swap Engine allows you to receive payments in the currency of your choice. For example, if someone sends you USDT and you want to build your BTC portfolio, you can enable the Easy Swap Engine so all the payments you receive will automatically be converted to BTC.

## Analysis

- Kasta states in their FAQ that they have a custodial provider, [Fireblocks](https://www.kasta.io/faq):
  > Our provider for custody is Fireblocks. We’re plugged into their API infrastructure that combines MPC-CMP with hardware isolation for multi-layered security.
- The app description and the statement above describe this app as a **custodial** service.
