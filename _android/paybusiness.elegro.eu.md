---
wsId: payBusinessElegro
title: elegro Business online banking
altTitle: 
authors:
- danny
users: 100
appId: paybusiness.elegro.eu
appCountry: 
released: 2020-07-15
updated: 2023-11-28
version: 1.3.3
stars: 
ratings: 
reviews: 
size: 
website: https://business.elegro.eu/elegro-business-wallet
repository: 
issue: 
icon: paybusiness.elegro.eu.jpg
bugbounty: 
meta: ok
verdict: fewusers
date: 2023-06-12
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: Niko Technologies
features: 

---

