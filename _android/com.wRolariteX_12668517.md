---
wsId: 
title: 'Noones: Buy Bitcoin & Giftcard'
altTitle: 
authors:
- danny
users: 1000
appId: com.wRolariteX_12668517
appCountry: 
released: 2021-09-08
updated: 2023-04-05
version: 1.0.62
stars: 3.7
ratings: 
reviews: 
size: 
website: https://www.bitcoin.rolarite.com
repository: 
issue: 
icon: com.wRolariteX_12668517.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-08-30
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: Rolasoft Auto Systems
features: 

---

## App Description from Google Play

> Exchange your gift card for Bitcoin and Cash at high rate we simplify all the ways you can sell and buy gift cards for cash and Bitcoin online easily. You can buy or sell just about any type of gift card including iTunes gift cards, Amazon gift cards, Google Play gift cards, Steam wallet gift cards, Sephora gift cards, Ebay and more for cash or Bitcoin instantly and securely. You can trade in any Currency of the world. You can sell as low as 5 face value of any gift card.

## Analysis 

- We installed the app, and it opened a browser to a website. 
- The interface was pretty crude which allows the user to "sell bitcoin" by inputting some numbers, for the provider to exchange to fiat presumedly, to deposit to the user's bank account. 

We did **not find a wallet** in this app.
