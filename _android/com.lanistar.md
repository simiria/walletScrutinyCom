---
wsId: lanistar
title: Lanistar
altTitle: 
authors:
- danny
users: 500000
appId: com.lanistar
appCountry: 
released: 2020-10-18
updated: 2023-08-24
version: 2.0.55
stars: 3
ratings: 
reviews: 60
size: 
website: https://www.lanistar.com
repository: 
issue: 
icon: com.lanistar.png
bugbounty: 
meta: ok
verdict: wip
date: 2023-07-10
signer: 
reviewArchive: 
twitter: iamlanistar
social:
- https://www.instagram.com/lanistar
redirect_from: 
developerName: Lanistar
features: 

---

## App Description from Google Play

> Lanistar is a banking service that gives you the power to manage ALL your money whether cash or crypto in one place on our App - giving you more control, protection and convenience. Not only that, it’s all backed up by 24/7 multilingual support.
>
> We've partnered with regulated crypto exchanges to bring you a full service to buy/ sell, hold, send and receive all in one place - so if you love crypto we have you covered!

## Analysis

- [(The Lanistar Terms and Conditions)](https://www.lanistar.com/terms-conditions/)
- We had difficulty in registering with this service because the sms verification did not arrive promptly. We changed our country of residence to UK, and the SMS to our temporary number worked.
- We registered, but had to wait for an activation code. We do not know if this is through email or sms. We already verified our email address. We waited to no avail.
- At the footer of the site, we found this:
  > Lanistar Limited ("Lanistar") is currently only carrying out pre-launch marketing in preparation for a future launch of Lanistar branded payment cards ("Cards"). Lanistar is finalising arrangements with various partner firms who are authorised and/or regulated (by the FCA and other overseas regulators) and the Cards will only be launched and go-live when those arrangements are in place.

- It would seem that the app has encountered regulatory hurdles with the FCA [(Link to 2023 news article)](https://www.uktech.news/fintech/lanistar-uk-return-20230419)
- Other [news reports](https://www.finextra.com/pressarticle/97150/lanistar-launches-crypto-in-brazil) indicate the cryptocurrency functionality is only available in Brazil.
- At the moment, there is nothing to indicate that this service currently has cryptocurrency functions apart from news reports.
- We emailed them at support@lanistar.com to inquire.
- For the meanwhile, we'll mark this as a **work-in-progress** until better access and further documentation is available.
